package com.tasks;

import java.util.Random;
import java.util.Scanner;

public class Game {
    public static Scanner in = new Scanner(System.in);

    public static void main(String[] args)  {
        Player player1 = new Player();
        Player player2 = new Player();
        System.out.println("Введите имя первого игрока: ");
        player1.setName(in.nextLine());
        System.out.println("Введите имя второго игрока: ");
        player2.setName(in.nextLine());
        while (player1.getHP() > 0 && player2.getHP() > 0) {
            round(player1, player2);
        }
        if (player1.getHP() <= 0) {
            System.out.println("Игрок " + player2.getName() + " победил");
        }
        if (player2.getHP() <= 0) {
            System.out.println("Игрок " + player1.getName() + " победил");
        }
    }

    public static void round(Player player1, Player player2) {
        System.out.println("Введите силу удара у первого игрока (от 1 до 9)");
        int player1Hit = in.nextInt();
        while (player1Hit < 1 || player1Hit > 9) {
            System.out.println("Сила удара должна быть от 1 до 9. Попробуйте еще раз");
            player1Hit = in.nextInt();
        }
        if (isMiss(player1Hit)) {
            player1.setHP(player1.getHP() - player1Hit);
        }
        System.out.println("Введите силу удара у второго игрока (от 1 до 9)");
        int player2Hit = in.nextInt();
        while (player2Hit < 1 || player2Hit > 9) {
            System.out.println("Сила удара должна быть от 1 до 9. Попробуйте еще раз");
            player2Hit = in.nextInt();
        }
        if (isMiss(player2Hit)) {
            player2.setHP(player2.getHP() - player2Hit);
        }
        System.out.println("Результаты раунда:");
        System.out.println("HP первого игрока: " + player1.getHP());
        System.out.println("HP второго игрока: " + player2.getHP());

    }

    public static boolean isMiss(int playerHit) {
        if (playerHit == 1) {
            return true;
        }
        Random r = new Random();
        int randomInt = r.nextInt(100) + 1;
        return randomInt - (playerHit * 10 - 10) >= 0;
    }
}


