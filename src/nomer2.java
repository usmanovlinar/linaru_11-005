import java.util.Scanner;

public class nomer2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int row = in.nextInt();
        int column = in.nextInt();
        int[][] a = new int[row][column];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                a[i][j] = in.nextInt();
            }
        }
        int det = 0;
        if (row == 2 && column == 2) {
            det = a[0][0] * a[1][1] - a[0][1] * a[1][0];
        }
        if (row == 3 && column == 3) {
            det = (a[0][0] * a[1][1] * a[2][2] + a[0][1] * a[1][2] * a[2][0] + a[1][0] * a[0][2] * a[2][1])  - (a[0][2] * a[1][1] * a[2][0] + a[1][0] * a[0][1] * a[2][2] + a[1][2] * a[2][1] * a[0][0]);
        }
        System.out.println(det);
    }
}
