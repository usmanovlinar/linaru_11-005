import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int row = in.nextInt();
        int column = in.nextInt();
        int[][] a = new int[row][column];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                a[i][j] = in.nextInt();
            }
        }
        for (int i = 0; i < column; i++) {
            for (int j = 0; j < column - i; j++) {
                if (j != column - i - 1) {
                    if (a[0][j] > a[0][j + 1]) {
                        for (int k = 0; k < row; k++) {
                            int temp = a[k][j];
                            a[k][j] = a[k][j + 1];
                            a[k][j + 1] = temp;
                        }
                    }
                }
            }
        }
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
}
